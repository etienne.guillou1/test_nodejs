const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
var GJ = mongoose.model('GJ');

passport.use(new localStrategy({
        usernameField: 'mail'
    },
    (username, password, done) => {
        GJ.findOne({ mail: username }, (err, user) => {
            if(err)
                return done(err);
            if(!user)
                return done(null, false, { message: 'GJ not found' });
            if(!user.validPassword(password))
                return done(null, false, { message: 'Wrong password' });
            
            return done(null, user); // Identification reussie
        });
    }
));