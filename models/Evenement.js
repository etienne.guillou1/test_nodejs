const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let EvenementSchema = mongoose.Schema({
    titre: {
        type: String,
        required: true
    }, 
    date: {
        type: String,
        required: true
    },
    lieu: {
        type: String,
        required: true
    },
    participants: {
        type:Number
    }
});

module.exports = mongoose.model('Evenement', EvenementSchema);