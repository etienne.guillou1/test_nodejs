const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/**
 * Modèle pour représenter les gilets jaunes qui participent à un événement
 */
let ParticipantSchema = mongoose.Schema({
    evenement: {
        type: Schema.Types.ObjectId, ref: 'Evenement',
        required : true
    }, 
    giletJaune: {
        type: Schema.Types.ObjectId, ref: 'GJ',
        required : true
    }
});

module.exports = mongoose.model('Participant', ParticipantSchema);