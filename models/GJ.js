const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

let GJSchema = mongoose.Schema({
    prenom: {
        type: String,
        required: true
    },
    nom: {
        type: String,
        required: true
    },
    mail: {
        type: String,
        required: true
    },
    age:Number,
    tel:String,
    role : {
        type: String,
        enum : {
            values:['CASSEUR', 'PROTECTION', 'MANIFESTANT'],
            message: "Les valeurs possibles pour le role sont 'CASSEUR', 'PROTECTION' ou 'MANIFESTANT'"
        },
        default: 'MANIFESTANT'
    },
    hash:String,
    salt:String
});

GJSchema.methods.display = function() {
    console.log(this);
}

/**
 * Méthode pour éviter de renvoyer le hash et le salt dans les infos
 */
GJSchema.methods.getInfos = function() {
    let giletjaune = {};
    
    giletjaune['_id'] = this._id;
    giletjaune['prenom'] = this.prenom;
    giletjaune['nom'] = this.nom;
    giletjaune['mail'] = this.mail;
    giletjaune['age'] = this.age;
    giletjaune['tel'] = this.tel;
    giletjaune['role'] = this.role;

    return giletjaune
}

/**
 * Securisation du mot de passe lors de l'ajout d'un nouveau gilet jaune
 * @param {*} password 
 */
GJSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};

/**
 * Méthode pour vérifier que le mot de passe entré par le gilet jaune est le bon
 * @param {} password 
 */
GJSchema.methods.validPassword = function(password) {
    let hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};

/**
 * Methode pour generer le token JWT
 */
GJSchema.methods.generateJwt = function() {
    let expiry = new Date();
    expiry.setDate(expiry.getDate() + 7); // Le token expire au bout de 7j

    return jwt.sign({
       _id: this._id,
       prenom: this.prenom,
       nom: this.nom,
       mail: this.mail,
       age: this.age,
       tel: this.tel,
       role: this.role,
       exp: parseInt(expiry.getTime() / 1000)
    }, 'SECRET');
};

module.exports = mongoose.model('GJ', GJSchema);