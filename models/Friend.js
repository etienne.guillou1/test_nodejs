const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let FriendSchema = mongoose.Schema({
    idGiletJaune1: {
        type: Schema.Types.ObjectId, ref: 'GJ',
        required: true
    }, 

    idGiletJaune2: {
        type: Schema.Types.ObjectId, ref: 'GJ',
        required : true
    }
});

module.exports = mongoose.model('Friend', FriendSchema);