Test Node.js - Etienne Guillou
=========================

Structure du code
------------

- Fichier principal du serveur :  `app.js`.
- Dossier `models` contient les modèles pour Mongoose.
- Dossier `routes` contient `routes.js` qui définit les différentes routes.
- Dossier `controllers` contient les fonctions appelées par les routes.
- Dossier `config` contient les configurations pour Passport.js

Lancement de l'application
------------
- `node app.js` pour lancer normalement.
- `npm run dev` ou `npm start` pour lancer avec `Nodemon`.

Fonctionnalités
------------

Un **Gilet Jaune** peut :
-  **S'inscrire**.
-  **Se connecter**.
-  **Accéder** à son profil.
-  **Accéder** au profil d'un autre gilet jaune.
-  **Modifier** ses informations.
-  **Ajouter** un autre gilet jaune à ses amis.
-  **Supprimer** un gilet jaune de ses amis.
-  **Créer** un événement.
-  **Voir** un événement.
-  **Participer** à un événement.

Tester les fonctionnalités
------------

> Toutes les fonctionnalités ont été testées avec Postman.


1.  **S'inscrire**  `/api/register` **(POST)**

Pour l'inscription, il faut renseigner les champs suivants dans le **body** d'une requête **POST** :
*  prenom
*  nom
*  mail
*  age
*  tel
*  role `'CASSEUR', 'PROTECTION' ou 'MANIFESTANT'`
*  password1
*  password2

`password1` doit être égal à `password2`. Si toutes les informations sont valides, le nouveau gilet jaune est maintenant inscrit.
Un **hash** est généré pour son mot de passe, pour ne pas le stocker en clair.


2.  **Se connecter**  `/api/login` **(POST)**

Pour la connexion, c'est encore une requête **POST** content les champs suivants dans le **body** :
* mail
* password

Si les identifiants sont bons, la connexion fonctionne et renvoie en réponse un **token** pour utiliser les routes de l'API accessibles qu'en étant connecté.
Il n'y a pas de route pour la **déconnexion** car dans l'idée, en utilisant un navigateur, lorsque le gilet jaune se connecte, son token est automatiquement stocké.
Pour le déconnecter, il suffit de lui désassocier ce token et on peut faire ça très bien avec Angular.

>  Les fonctionnalités qui suivent nécéssitent d'être connecté, donc que le **token** soit envoyé dans la requête. Avec **Postman** par exemple, on peut le lier en
se rendant dans l'onglet ***Authorization***, dans ***TYPE*** on choisit ***Bearer Token*** puis on le renseigne dans le champ ***Token*** à droite.

3.  **Accéder à son profil**  `/api/profile` **(GET) + token** 

Ici il suffit de faire une requête **GET** sur cette route sans paramètre mais en renseignant le **token** et l'API nous retournera les informations du gilet jaune
connecté.

4.  **Mettre à jour son profil**  `/api/profile` **(POST) + token** 

Pour mettre à jour son profil, il faut renseigner dans le **body** de la requête les champs qu'on désire modifier en leur attribuant leur nouvelle valeur.
Ces champs peuvent être :
*  prenom
*  nom
*  mail
*  age
*  tel
*  role `'CASSEUR', 'PROTECTION' ou 'MANIFESTANT'`

5.  **Accéder au profil d'un autre gilet jaune**  `/api/user/:id` **(GET) + token** 

C'est une requête **GET** où il suffit de renseigner l'ID du gilet jaune dont on veut voir le profil, par exemple :
`/api/user/5c756c333e8d6125a8f7c320`

6.  **Ajouter un gilet jaune à ses amis**  `/api/addfriend/:id` **(GET) + token** 

Comme la requête précédente, c'est une requête **GET** où il suffit de renseigner l'ID du gilet jaune qu'on veut en ami, par exemple :
`/api/addfriend/5c756c333e8d6125a8f7c320`
Si les deux gilets jaunes ne sont pas déjà amis, ça les ajoute en ami. Bon, ça fonctionne sans le consentement de celui qui est demandé en ami mais dans l'idée, c'était assez simple
de faire comme ça !

7.  **Supprimer un gilet jaune de ses amis**  `/api/removefriend/:id` **(GET) + token** 

Même fonctionnement que la requête pour ajouter en ami ! Elle fonctionne si les deux gilets jaunes sont déjà amis.

8.  **Ajouter un événement**  `/api/evenement` **(POST) + token** 

L'ajout d'un événement par un gilet jaune se fait grâce à une requête **POST** avec en **body** les champs :
* titre
* date
* lieu

Au final j'aurais pu ajouter l'auteur aussi, qui correspondrait au gilet jaune qui a créé l'événement.

9.  **Voir un événement**  `/api/evenement/:id` **(GET) + token** 

Simple requête **GET** avec en paramètre l'ID de l'événement qu'on veut voir.
La requête permet également de voir le **nombre de participants** de cet événement.

10.  **Participer à un événement**  `/api/addparticipant` **(POST) + token** 

Cette requête permet de s'ajouter à la liste des participants de l'événement. Ici le seul paramètre à renseigner dans le **body** est :
* evenement (l'ID de l'événement)

On ne peut biensûr pas rejoindre plusieurs fois le même événement.

L'ID du gilet jaune sera ajouté automatiquement. Il correspond à l'ID du gilet jaune actuellement connecté.