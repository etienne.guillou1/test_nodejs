const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config/database');
const passport = require('passport');

// appels des modeles (a faire avant les routes)
require('./models/GJ');
require('./models/Friend');
require('./models/Evenement');
require('./models/Participant');

// ajout de la config de Passport
require('./config/passport');

// appels des routes de l'API
const routesApi = require('./routes/routes');

const app = express();
const port = process.env.PORT || '3000';

// Connexion de mongoose a la base de donnees
/*const options = { useNewUrlParser : true };
mongoose.connect(config.database, options);
mongoose.set('debug', true);*/

mongoose.connect('mongodb://localhost:27017/appartoo', {useNewUrlParser: true});

// Passport setup
app.use(passport.initialize());

//Middleware CORS
app.use(cors());

// Configuration de body-parser
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

/* Pour utiliser angular sur le port 3000 */
//app.use(express.static(path.join(__dirname, 'angular-src/dist/angular-src')));

/* ROUTING */
app.use('/api', routesApi); // Lien vers la liste des routes de l'API (a mettre avant le get '*' pour angular !!)
/*app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'angular-src/dist/angular-src/index.html'));
});
*/

app.use((err, req, res, next) => {
  if(err.name === 'UnauthorizedError'){
      res.status(401);
      res.json({ message: err.name + ": " + err.message });
  }
});

app.listen(port, () => console.log(`API running on port ${port}`));