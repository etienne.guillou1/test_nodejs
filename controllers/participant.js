const mongoose = require('mongoose');
const Evenement = mongoose.model('Evenement');
const Participant = mongoose.model('Participant');

module.exports.addParticipant = async (req, res) => {

    if(req.body.evenement) {
        if(mongoose.Types.ObjectId.isValid(req.body.evenement)) {

            let evenement = await Evenement.findById(req.body.evenement);
            if(evenement) { // Si l'evenement existe bien
                let participe = await Participant.find({
                    evenement:req.body.evenement,
                    giletJaune: req.payload._id
                });

                if(!participe[0]) { // On verifie que le gilet jaune ne participe pas deja a l'evenement

                    let participant = new Participant({
                        evenement:req.body.evenement,
                        giletJaune: req.payload._id
                    });

                    let result = await participant.save();
                    result ? res.json({success:true}) : res.json({success:false});

                    return;
                }
            }
        }
        res.json({success:false});
    }
    else
        res.json({ success:false, message:"Tous les champs doivent être remplis"});
};