const passport = require('passport');
const mongoose = require('mongoose');
const GJ = mongoose.model('GJ');

module.exports.login = (req, res) => {
    passport.authenticate('local', (err, giletjaune, info) => {
        let token;
        if(err) { // utilisateur inconnu
            res.status(404).json(err);
            return;
        }

        if(giletjaune){ // connexion ok
            token = giletjaune.generateJwt();
            res.status(200);
            res.json({ success:true, token: token });
        }
        else { // mauvais mot de passe
            res.status(401).json(info);
        }
    })(req, res);
};

module.exports.register = (req, res, next) => {

    // Vérification que l'utilisateur a rempli les champs
    let formFields = [
        { key: "prenom", value: "Prénom"},
        { key: "nom", value: "Nom" },
        { key: "mail", value: "Mail"},
        { key: "age", value: "Age"},
        { key: "tel", value: "Téléphone"},
        { key: "role", value: "Rôle"},
        { key: "password1", value: "Mot de passe 1"},
        { key: "password2", value: "Mot de passe 2"}
    ];

    for(let field of formFields) {
        if(!req.body[field.key])
            res.json({ success: false, field:field.key, msg: `Le champ ${field.value} est obligatoire !`});
    }

    // Création du nouveau Gilet Jaune
    let giletJaune = new GJ();

    giletJaune.prenom = req.body.prenom;
    giletJaune.nom = req.body.nom;
    giletJaune.mail = req.body.mail;
    giletJaune.age = req.body.age;
    giletJaune.tel = req.body.tel;
    giletJaune.role = req.body.role

    // On verifie que les mots de passe sont les memes
    if(req.body.password1 === req.body.password2)
        req.body.password = req.body.password1;
    else
        res.json({ success: false, msg:'Les mots de passe sont différents' });

    GJ.findOne({ mail:giletJaune.mail }, (err, userfound) => {
        if(err)
            res.json( {success: false, msg: err });
        if(userfound) // un utilisateur possede déjà cet email
            res.json( {success: false, msg: 'L\'adresse mail est déjà utilisée' });
        else {
            giletJaune.setPassword(req.body.password); // on definit le hash pour son mot de passe
            giletJaune.save((err, gj) => {
                if(err)
                    res.json({success:false, msg:err.errors.role.message});
                else
                    res.json({ success: true, giletjaune:gj });
            });
        }
    });
}

/**
 * Pour accéder à son propre profil (nécéssite d'être connecté)
 * @param {*} req 
 * @param {*} res 
 */
module.exports.getProfile = (req, res) => {
    if(!req.payload._id) // pas d'ID utilisateur existe dans le JWT
        res.status(401).json({ success:false, message: "Vous devez être connecté pour accéder à cette page" });
    else {
        GJ
        .findById(req.payload._id)
        .exec((err, user) => {
            res.status(200).json(user);
        });
    }
};

/**
 * Pour accéder au profil d'un autre gilet jaune enregistré (nécéssite d'être connecté)
 * @param {*} req 
 * @param {*} res 
 */
module.exports.getUser = (req, res) => {
    let id = req.params.id;
    GJ
    .findById(id)
    .exec((err, user) => {
        if(err)
            res.json({success: false, message: "Ce gilet jaune n'existe pas" });
        else
            res.status(200).json(user.getInfos());
    });
};

/**
 * Mise à jour du profil du gilet jaune connecté
 * @param {*} req 
 * @param {*} res 
 */
module.exports.updateProfile = (req, res) => {

    toUpdate = {};

    if(req.body.prenom)
        toUpdate['prenom'] = req.body.prenom;
    if(req.body.nom)
        toUpdate['nom'] = req.body.nom;
    if(req.body.mail)
        toUpdate['mail'] = req.body.mail;
    if(req.body.age)
        toUpdate['age'] = req.body.age;
    if(req.body.tel)
        toUpdate['tel'] = req.body.tel;
    if(req.body.role)
        toUpdate['role'] = req.body.role;

    GJ.updateMany({ _id: req.payload._id }, { $set: toUpdate }, (err, updateInfos) => {
        res.json({ success:true, updateInfos:updateInfos });
    });
}