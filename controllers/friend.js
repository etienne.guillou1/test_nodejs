const mongoose = require('mongoose');
const GJ = mongoose.model('GJ');
const Friend = mongoose.model('Friend');

module.exports.addFriend = async (req, res) => {
    let id = req.params.id;
    let exist = await friendshipExists(id, req.payload._id);

    if(!exist) { // On verifie que cette amitié n'existe pas déjà
        let giletJauneToAdd = await GJ.find({ _id: id });
        console.log("1")

        if(giletJauneToAdd.length > 0) { // le gilet jaune qu'on demande en ami existe bien
            if(giletJauneToAdd[0]._id !== id) { // on ne fait pas une demande d'amitié à nous même
            console.log("2")

                let friendship = new Friend({
                    idGiletJaune1: req.payload._id, // Gilet jaune courant
                    idGiletJaune2: id // Gilet jaune ajouté
                });

                let newFriendship = await friendship.save();
                res.json({ success:true, newFriendship:newFriendship });
                
            }
            else
                res.json({ success:false, message:"Vous ne pouvez pas vous ajouter vous même en ami"});

            return;
        }
    }
    res.json({ success:false });
};

module.exports.removeFriend = async (req, res) => {
    //let result = await friendshipExists("5c756e49dd41663e5c5d61b7", "5c756c333e8d6125a8f7c320");
    let id = req.params.id;

    /* On vérifie d'abord que cette amitié existe bien déjà. Si elle existe alors
    on sait que les deux gilets jaunes existent et que ce n'est pas la même personne,
    ça nous évite quelques vérifications */

    let exist = friendshipExists(id, req.payload._id);
    if(exist) {

        let friendshipRemoved1 = await Friend.findOneAndDelete({
            idGiletJaune1: req.payload._id,
            idGiletJaune2: id
        });
        let friendshipRemoved2 = await Friend.findOneAndDelete({
            idGiletJaune1: id,
            idGiletJaune2: req.payload._id
        });

        if(friendshipRemoved1 || friendshipRemoved2) {
            res.json({ success:true, message:"Ami supprimé !"});
            return;
        }
    }
    res.json({success: false});
};
/**
 * Retourne true si l'amitié entre les deux gilets jaunes en paramètre
 * existe, false sinon.
 * On doit vérifier les deux sens pour l'amitié car il y a une unique entrée
 * pour deux gilets jaunes.
 * @param {*} giletJauneId1 
 * @param {*} giletJauneId2 
 */
const friendshipExists = async (giletJauneId1, giletJauneId2) => {

    if(!(mongoose.Types.ObjectId.isValid(giletJauneId1) && mongoose.Types.ObjectId.isValid(giletJauneId2)))
        return false;

    let friendshipV1 = await Friend.find({
        idGiletJaune1: giletJauneId1,
        idGiletJaune2: giletJauneId2
    });

    let friendshipV2 = await Friend.find({
        idGiletJaune1: giletJauneId2,
        idGiletJaune2: giletJauneId1
    });

    return friendshipV1.length > 0 || friendshipV2.length > 0 ;
}