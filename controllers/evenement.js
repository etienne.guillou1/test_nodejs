const mongoose = require('mongoose');
const Evenement = mongoose.model('Evenement');
const Participant = mongoose.model('Participant');

module.exports.createEvent = async (req, res) => {

    if(req.body.titre && req.body.date && req.body.lieu) {
        let evenement = new Evenement({
            titre:req.body.titre,
            date:req.body.date,
            lieu:req.body.lieu
        });

        let newEvent = await evenement.save();

        res.json({success:true, newEvent: newEvent});
    }
    else
        res.json({ success:false, message:"Tous les champs doivent être remplis"});
};

/**
 * Retourne un evenement correspondant à l'ID en paramètre.
 * L'evenement retourné possède egalement le nombre de participants
 * @param {*} req 
 * @param {*} res 
 */
module.exports.viewEvent = async (req, res) => {
    let id = req.params.id;

    if(mongoose.Types.ObjectId.isValid(id)) {
        let event = await Evenement.findById(id,);
        let countParticipants = await Participant.countDocuments({evenement:id});

        event['participants'] = countParticipants;

        event ? res.json({ success:true, event: event }) : res.json({ success:false, message: "Cet événement n'existe pas "});
        return;
    }

    res.json({ success:false });
}