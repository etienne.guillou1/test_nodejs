const express = require('express');
const jwt = require('express-jwt');
const router = express.Router();

const userController = require('../controllers/user');
const friendController = require('../controllers/friend');
const evenementController = require('../controllers/evenement');
const participantController = require('../controllers/participant');

const auth = jwt({
    secret: 'SECRET', // ici on n'est pas censé le mettre en clair, dans l'ideal dans une variable d'env : process.env.JWT_SECRET
    userProperty: 'payload'
});

router.post('/register', userController.register); // Inscription
router.post('/login', userController.login); // Connexion
router.get('/profile', auth, userController.getProfile); // Acces a son profil
router.post('/profile', auth, userController.updateProfile); // Mise a jour du profil
router.get('/user/:id', auth, userController.getUser); // Acces au profil d'un autre gilet jaune
router.get('/addfriend/:id', auth, friendController.addFriend); // Ajoute un gilet jaune en ami
router.get('/removefriend/:id', auth, friendController.removeFriend); // Enleve un gilet jaune de ses amis
router.post('/evenement', auth, evenementController.createEvent); // Ajoute un evenement
router.get('/evenement/:id', auth, evenementController.viewEvent); // Voir un evenement
router.post('/addparticipant', auth, participantController.addParticipant); // Ajoute un participant à un evenement

module.exports = router;